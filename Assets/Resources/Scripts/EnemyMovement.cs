﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public float speed;

    private Transform _target;
    private int _waypointIndex = 0;

    private void Start()
    {
        _target = Waypoints.waypoints[0];
        //Make the enemy look at the waypoint
        transform.right = _target.position - transform.position;
    }

    private void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position,
            _target.transform.position,
            speed * Time.deltaTime);

        if(Vector2.Distance(transform.position, _target.position) <= 0.2f)
        {
            GetNextWaypoint();
        }
    }

    private void GetNextWaypoint()
    {
        if(_waypointIndex >= Waypoints.waypoints.Length - 1)
        {
            Destroy(gameObject);
            return;
        }

        _waypointIndex++;
        _target = Waypoints.waypoints[_waypointIndex];
        transform.right = _target.position - transform.position;
    }
}

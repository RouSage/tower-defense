﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Node : MonoBehaviour
{
    public Color hoverColor;                        //Color of the node on mouse enter
    public Color forbiddingColor;                   //Color which shows that something wrong and you can't build
    private Color _initialColor;                    //Stores the initial color of the node
    private SpriteRenderer _spriteRenderer;         //Reference to the sprite renderer component

    [Header("Optional")]
    public GameObject turret;                       //Reference to the turret which is built on this node

    BuildManager buildManager;

    private void Awake()
    {
        //Setting up the references
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        // Store the initial color of the Node so we can return it when needed
        _initialColor = _spriteRenderer.color;
        // Getting the BuildManage instance
        buildManager = BuildManager.instance;
        // Check if there's already a turret on this Node so it'll disable the sprite
        UpdateSprite();
    }

    private void UpdateSprite()
    {
        if(turret != null)
        {
            _spriteRenderer.enabled = false;
        }
        else
        {
            _spriteRenderer.enabled = true;
        }
    }

    private void OnMouseDown()
    {
        // If UI hovers over the Node GameObject then it's impossible to use this Node
        if (EventSystem.current.IsPointerOverGameObject()) return;

        // If there's no turret to build we won't place anything on the Node
        if (!buildManager.CanBuild) return;

        //If there's a turret on this Node already
        if(turret != null)
        {
            //TODO: Dispaly on screen (UI)
            Debug.Log("Can't build there!");
            return;
        }

        //Else build the turret on this node
        buildManager.BuildTurretOn(this);
        UpdateSprite();
    }

    private void OnMouseEnter()
    {
        // If UI hovers over the Node GameObject then it's impossible to use this Node
        if (EventSystem.current.IsPointerOverGameObject()) return;

        // If there's no bought turret to build then the Node won't be highlighted
        if (!buildManager.CanBuild) return;

        // If there's enough money to build the turret
        if (buildManager.HasMoney)
        {
            // Change the color of the Node to the hover color
            _spriteRenderer.color = hoverColor;
        }
        else
        {
            // Show that player can't build by changing the color
            _spriteRenderer.color = forbiddingColor;
        }
    }

    private void OnMouseExit()
    {
        // Return the initial color of the Node
        _spriteRenderer.color = _initialColor;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildManager : MonoBehaviour
{
    public static BuildManager instance;            //Singleton use
    public Text goldText;                           //Reference to the Text UI element which contains information about player's gold

    private TurretBlueprint _turretToBuild;         //The turret GameObject which is gonna be built

    public bool CanBuild { get { return _turretToBuild != null; } }
    public bool HasMoney { get { return PlayerStats.Money >= _turretToBuild.cost; } }

    private void Awake()
    {
        if(instance != null)
        {
            Debug.LogError("More than one BuildManager in the scene!");
            return;
        }

        instance = this;
    }

    /// <summary>
    /// Called when one of the items in the Shop was clicked (bought)
    /// Sets the next turret to build to turret parameter
    /// </summary>
    /// <param name="turret">The turret that will be built next</param>
    public void SelectTurretToBuild(TurretBlueprint turret)
    {
        _turretToBuild = turret;
    }

    /// <summary>
    /// Instantiates the turret on the Node
    /// </summary>
    /// <param name="node">Node where the turret is gonna be built</param>
    public void BuildTurretOn(Node node)
    {
        // Only if the player has enough money to buy a turret
        if(PlayerStats.Money < _turretToBuild.cost)
        {
            Debug.Log("Not enough money to build this turret!");
            return;
        }

        // Subtract the player's money
        PlayerStats.Money -= _turretToBuild.cost;

        // Update the GUI information about player's gold
        goldText.text = PlayerStats.Money.ToString();

        // Placing turret at the node's position
        GameObject turretGO = Instantiate(_turretToBuild.prefab, node.transform.position, Quaternion.identity);
        node.turret = turretGO;

        Debug.Log("Turret was built! Money left: " + PlayerStats.Money);
    }
}

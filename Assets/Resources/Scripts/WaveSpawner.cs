﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveSpawner : MonoBehaviour
{
    public enum SpawnState { SPAWNING, WAITING, COUNTING }     //States of the spawning process

    [System.Serializable]
    public class Wave
    {
        public string name;
        public Transform[] enemies;
        public float rate;
    }

    public Transform spawnPoint;                //Enemies spawn at this point

    public Wave[] waves;                        //Array of waves to spawn
    private int _nextWave = 0;                  //Index of the wave to spawn next

    public float timeBetweenWaves = 5f;         //Time to wait to start spawn next wave
    private float _waveCountdown;               //Countdown to each wave spawn

    private float _searchCountdown = 1f;        //Every second script'll check for the alive enemies

    public Text waveCountdownText;              //Reference to the UI Text element which shows the amount of time 'till the next wave

    private SpawnState _state = SpawnState.COUNTING;

    private void Start()
    {
        _waveCountdown = timeBetweenWaves;
    }

    private void Update()
    {
        if(_state == SpawnState.WAITING)
        {
            //Checking if the enemies aren't still alive
            if (!IsEnemyAlive())
            {
                //Begin a new round
                Debug.Log("Wave completed!");
                OnWaveCompleted();
            }
            else
            {
                return;
            }
        }

        if(_waveCountdown <= 0)
        {
            if(_state != SpawnState.SPAWNING)
            {
                // Start spawning a wave
                StartCoroutine(SpawnWave(waves[_nextWave]));
            }
        }
        else
        {
            _waveCountdown -= Time.deltaTime;

            // Clamp the _waveCountdown value so it won't be less than a zero
            _waveCountdown = Mathf.Clamp(_waveCountdown, 0, Mathf.Infinity);
        }

        // Show the current amount of time on the UI Text element
        waveCountdownText.text = string.Format("{0:00.00}", _waveCountdown);
    }

    private bool IsEnemyAlive()
    {
        _searchCountdown -= Time.deltaTime;

        if(_searchCountdown <= 0f)
        {
            //Check if there're alive enemies
            if (GameObject.FindGameObjectWithTag("Enemy") == null)
            {
                return false;
            }
        }

        return true;
    }

    private void OnWaveCompleted()
    {
        //Current state return to the default
        _state = SpawnState.COUNTING;
        //Count down from the beginning
        _waveCountdown = timeBetweenWaves;

        //If all waves were spawned
        if(_nextWave + 1 > waves.Length - 1)
        {
            _nextWave = 0;
            // TODO: End of the level
            Debug.Log("All waves completed!");
        }
        else
        {
            _nextWave++;
        }
    }

    IEnumerator SpawnWave(Wave wave)
    {
        //Changing the current state to SPAWNING
        _state = SpawnState.SPAWNING;

        //Spawn enemy
        for (int i = 0; i < wave.enemies.Length; i++)
        {
            Instantiate(wave.enemies[i], spawnPoint.position, Quaternion.identity);
            yield return new WaitForSeconds(1f / wave.rate);
        }

        _state = SpawnState.WAITING;

        yield break;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Camera _camera;

    [Header("Camera Movement")]
    public float panSpeed = 2f;
    public float panBorderThickness = 10f;

    private bool _doMovement = true;
    
    [Header("Camera Scrolling")]
    public float scrollSpeed = 100f;
    public float minCamSize;
    public float maxCamSize;

    private void Awake()
    {
        _camera = gameObject.GetComponent<Camera>();
    }

    private void Update()
    {
        // Escape key disables Camera movement and scrolling
        if (Input.GetKey(KeyCode.Escape))
        {
            _doMovement = !_doMovement;
        }

        if (!_doMovement) return;

        // Control camera movement with the WASD keys and the mouse
        if (Input.GetKey("w") || Input.mousePosition.y >= Screen.height - panBorderThickness)
        {
            transform.Translate(Vector2.up * panSpeed * Time.deltaTime);
        }
        if (Input.GetKey("s") || Input.mousePosition.y <= panBorderThickness)
        {
            transform.Translate(Vector2.down * panSpeed * Time.deltaTime);
        }
        if (Input.GetKey("d") || Input.mousePosition.x >= Screen.width - panBorderThickness)
        {
            transform.Translate(Vector2.right * panSpeed * Time.deltaTime);
        }
        if (Input.GetKey("a") || Input.mousePosition.x <= panBorderThickness)
        {
            transform.Translate(Vector2.left * panSpeed * Time.deltaTime);
        }

        //Scrolling
        float scroll = Input.GetAxis("Mouse ScrollWheel");
        
        float cameraSize = _camera.orthographicSize;
        cameraSize -= scroll * scrollSpeed * Time.deltaTime;
        cameraSize = Mathf.Clamp(cameraSize, minCamSize, maxCamSize);
        _camera.orthographicSize = cameraSize;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretShooting : MonoBehaviour
{
    public float fireRate;                          //Firerate of the turret
    private float _fireCountdown = 0f;              //Countdown for shots

    public GameObject shellPrefab;                  //Reference to the shell prefab
    public GameObject[] shells;                     //Contains instantiated shells
    public GameObject[] gunfires;                   //Reference to the gunfire prefab
    public Transform[] shellSpawns;                 //Reference to the shells' spawnpoints

    private Transform _target;                      //Reference to the target which we take from the Turret script
    private int _index = 0;

    private void Update()
    {
        //Shooting starts when time is up and there's a target to shoot
        if (_fireCountdown <= 0f && _target != null)
        {
            if (shells.Length > 1)
            {
                if (_index >= shells.Length)
                    _index = 0;

                // It's here until all turrets won't get gunfire sprites
                if (gunfires.Length != 0) { StartCoroutine(Gunfire(_index)); }
                StartCoroutine(Shoot(_index));
                _index++;
            }
            else
            {
                if (gunfires.Length != 0) { StartCoroutine(Gunfire(_index)); }
                StartCoroutine(Shoot(_index));
            }

            _fireCountdown = 1f / fireRate;
        }
        
        _fireCountdown -= Time.deltaTime;
    }

    private IEnumerator Shoot(int index)
    {
        if (shells[index] != null)
        {
            shells[index].GetComponent<Shell>().enabled = true;
            shells[index].GetComponent<Shell>().SetTarget(_target);
        }

        yield return new WaitForSeconds(1f);

        shells[index] = Instantiate(shellPrefab, shellSpawns[index].position, shellSpawns[index].rotation, shellSpawns[index].parent);

        yield break;
    }

    /// <summary>
    /// Coroutine for activating and deactivating GunFire sprites
    /// </summary>
    /// <param name="index">Index of Gunfire whose state need to switch</param>
    /// <returns></returns>
    private IEnumerator Gunfire(int index)
    {
        gunfires[index].SetActive(true);

        yield return new WaitForSeconds(0.15f);

        gunfires[index].SetActive(false);

        yield break;
    }

    /// <summary>
    /// Sets the target for shooting
    /// </summary>
    /// <param name="target">Transform of the target</param>
    public void SetTarget(Transform target)
    {
        _target = target;
    }
}

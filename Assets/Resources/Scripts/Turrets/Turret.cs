﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    public float range;                         //View range of the turret
    [HideInInspector]
    private Transform _target;                   //Game object in which the turret will try to shoot
    public string enemyTag = "Enemy";           //Tag for finding the enemies 

    public float turnSpeed;                     //Turret's turning speed

    public Transform partToRotate;              //Part of the turret which will rotate

    private void Start()
    {
        //Updating the turret's target every 0.5 seconds
        InvokeRepeating("UpdateTarget", 0f, 0.5f);
    }

    private void UpdateTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;

        foreach (var enemy in enemies)
        {
            float distanceToEnemy = Vector2.Distance(transform.position, enemy.transform.position);
            if(distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }

        if(nearestEnemy != null && shortestDistance <= range)
        {
            _target = nearestEnemy.transform;
            gameObject.GetComponent<TurretShooting>().SetTarget(_target);
        }
        else
        {
            _target = null;
        }
    }

    private void Update()
    {
        if (_target == null) return;

        //Target lock on
        Vector2 direction = _target.position - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
        partToRotate.rotation = Quaternion.Lerp(partToRotate.rotation, Quaternion.Euler(0f, 0f, angle),
            turnSpeed * Time.deltaTime);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}

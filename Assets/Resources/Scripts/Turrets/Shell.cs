﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shell : MonoBehaviour
{
    private Transform _target;

    public float speed = 5f;
    public float explosionRadius = 0f;          //Keep it zero if no need of explosive shell

    public void SetTarget(Transform target)
    {
        _target = target;
    }

    private void Update()
    {
        if(_target == null)
        {
            Destroy(gameObject);
            return;
        }

        transform.position = Vector2.MoveTowards(transform.position,
            _target.transform.position,
            speed * Time.deltaTime);

        if (Vector2.Distance(transform.position, _target.position) <= 0.1f)
        {
            HitTarget();
            return;
        }
    }

    private void HitTarget()
    {
        if(explosionRadius > 0f)
        {
            Explode();
        }
        else
        {
            Damage(_target);
        }

        Destroy(gameObject);
    }

    private void Explode()
    {
        //TODO: Maybe need to use a layer mask instead
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, explosionRadius);

        foreach (var collider in colliders)
        {
            if(collider.tag == "Enemy")
            {
                Damage(collider.transform);
            }
        }
    }

    private void Damage(Transform enemy)
    {
        Destroy(enemy.gameObject);
    }

    // Shows the explosion radius of each shell in the Editor when it's selected
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }
}
